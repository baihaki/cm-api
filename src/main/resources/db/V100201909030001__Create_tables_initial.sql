﻿DROP EXTENSION IF EXISTS "uuid-ossp";
CREATE EXTENSION "uuid-ossp" SCHEMA public VERSION "1.0";

CREATE TABLE cm_comm_type
(
  id bigint NOT NULL,
  text character varying(16),
  CONSTRAINT cm_comm_type_pkey PRIMARY KEY (id)
);
CREATE SEQUENCE comm_type_serial OWNED BY cm_comm_type.id;
ALTER TABLE ONLY cm_comm_type ALTER COLUMN id SET DEFAULT nextval('comm_type_serial'::regclass);
COMMENT ON COLUMN cm_comm_type.id IS 'ID of Communication Type';
COMMENT ON COLUMN cm_comm_type.text IS 'Communication Type Text/Description';

CREATE TABLE cm_comm
(
  id character(13) NOT NULL DEFAULT left((uuid_generate_v4())::text, 13),
  comm_type bigint NOT NULL,
  comm_value character varying(64),
  preferred boolean,
  CONSTRAINT cm_comm_pkey PRIMARY KEY (id),
  CONSTRAINT cm_comm_comm_type_fkey FOREIGN KEY (comm_type)
      REFERENCES cm_comm_type (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT
);
COMMENT ON COLUMN cm_comm.id IS 'ID of Communication';
COMMENT ON COLUMN cm_comm.comm_type IS 'Communication Type';
COMMENT ON COLUMN cm_comm.comm_value IS 'Communication Value/Description';
COMMENT ON COLUMN cm_comm.preferred IS 'Preferred Communication (True/False)';

CREATE TABLE cm_addr_type
(
  id bigint NOT NULL,
  text character varying(16),
  CONSTRAINT cm_addr_type_pkey PRIMARY KEY (id)
);
CREATE SEQUENCE addr_type_serial OWNED BY cm_addr_type.id;
ALTER TABLE ONLY cm_addr_type ALTER COLUMN id SET DEFAULT nextval('addr_type_serial'::regclass);
COMMENT ON COLUMN cm_addr_type.id IS 'ID of Address Type';
COMMENT ON COLUMN cm_addr_type.text IS 'Adddress Type Text/Description';

CREATE TABLE cm_addr
(
  id character(13) NOT NULL DEFAULT left((uuid_generate_v4())::text, 13),
  addr_type bigint NOT NULL,
  street_number int,
  street character varying(64),
  unit character varying(5),
  city character varying(32),
  state_code character varying(2),
  zip character varying(6),
  CONSTRAINT cm_addr_pkey PRIMARY KEY (id),
  CONSTRAINT cm_addr_addr_type_fkey FOREIGN KEY (addr_type)
      REFERENCES cm_addr_type (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT
);
COMMENT ON COLUMN cm_addr.id IS 'ID of Address';
COMMENT ON COLUMN cm_addr.addr_type IS 'Address Type';

CREATE TABLE cm_identity
(
  id character(13) NOT NULL DEFAULT left((uuid_generate_v4())::text, 13),
  fname character varying(32),
  lname character varying(32),
  dob date,
  gender character(1),
  title character varying(32),
  CONSTRAINT cm_identity_pkey PRIMARY KEY (id),
  CONSTRAINT cm_identity_gender_check CHECK (gender::text = ANY (ARRAY['M', 'F']))
);
COMMENT ON COLUMN cm_identity.id IS 'ID of Identity';

CREATE TABLE cm_identity_addr
(
  identity_id character varying(13) NOT NULL,
  addr_id character varying(13) NOT NULL,
  CONSTRAINT cm_identity_addr_pkey PRIMARY KEY (identity_id, addr_id),
  CONSTRAINT cm_identity_addr_identity_id_fkey FOREIGN KEY (identity_id)
      REFERENCES cm_identity (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT,
  CONSTRAINT cm_identity_addr_addr_id_fkey FOREIGN KEY (addr_id)
      REFERENCES cm_addr (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT
);

CREATE TABLE cm_identity_comm
(
  identity_id character varying(13) NOT NULL,
  comm_id character varying(13) NOT NULL,
  CONSTRAINT cm_identity_comm_pkey PRIMARY KEY (identity_id, comm_id),
  CONSTRAINT cm_identity_comm_identity_id_fkey FOREIGN KEY (identity_id)
      REFERENCES cm_identity (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT,
  CONSTRAINT cm_identity_comm_comm_id_fkey FOREIGN KEY (comm_id)
      REFERENCES cm_comm (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT
);