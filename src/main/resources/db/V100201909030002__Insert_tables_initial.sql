﻿INSERT INTO cm_addr_type (text) VALUES ('home');
INSERT INTO cm_addr_type (text) VALUES ('work');
INSERT INTO cm_addr_type (text) VALUES ('emergency');

INSERT INTO cm_comm_type (text) VALUES ('email');
INSERT INTO cm_comm_type (text) VALUES ('cell');
INSERT INTO cm_comm_type (text) VALUES ('skype');

INSERT INTO cm_addr (addr_type, street_number, street, unit, city, state_code, zip) VALUES (1, 1234, 'Blah Blah St', '1 a', 'Somewhere', 'WV', '12345');
INSERT INTO cm_addr (addr_type, street_number, street) VALUES (2, 65, 'Highway Rd');
INSERT INTO cm_addr (addr_type, street_number, street) VALUES (3, 17, 'Neighbour Elm St');
INSERT INTO cm_addr (addr_type, street_number, street) VALUES (1, 99, 'Higher St');

INSERT INTO cm_comm (comm_type, comm_value, preferred) VALUES (1, 'bfe@sample.com', true);
INSERT INTO cm_comm (comm_type, comm_value) VALUES (2, '304-555-8282');
INSERT INTO cm_comm (comm_type, comm_value) VALUES (3, 'baihakis');

INSERT INTO cm_identity (fname, lname, dob, gender, title) VALUES ('Bob', 'Frederick', '1980-06-21', 'M', 'Manager');
INSERT INTO cm_identity (fname, lname, dob, gender, title) VALUES ('Ahmad', 'Baihaki', '1945-08-17', 'M', 'Human');
INSERT INTO cm_identity (fname, lname, dob, gender, title) VALUES ('Ahmad', 'Taufik', '1960-08-17', 'M', 'Human');

INSERT INTO cm_identity_addr VALUES ((SELECT id FROM cm_identity WHERE lname='Frederick'), (SELECT id FROM cm_addr WHERE street_number=1234));
INSERT INTO cm_identity_addr VALUES ((SELECT id FROM cm_identity WHERE lname='Baihaki'), (SELECT id FROM cm_addr WHERE street_number=17));
INSERT INTO cm_identity_addr VALUES ((SELECT id FROM cm_identity WHERE lname='Baihaki'), (SELECT id FROM cm_addr WHERE street_number=99));

INSERT INTO cm_identity_comm VALUES ((SELECT id FROM cm_identity WHERE lname='Frederick'), (SELECT id FROM cm_comm WHERE comm_value='bfe@sample.com'));
INSERT INTO cm_identity_comm VALUES ((SELECT id FROM cm_identity WHERE lname='Frederick'), (SELECT id FROM cm_comm WHERE comm_value='304-555-8282'));
INSERT INTO cm_identity_comm VALUES ((SELECT id FROM cm_identity WHERE lname='Baihaki'), (SELECT id FROM cm_comm WHERE comm_value='baihakis'));