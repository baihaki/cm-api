# README #

Contacts Management API -> POC of Back-end RESTFul Service developed with Anypoint Studio 7 and Mulesoft 4.

* * *

## How do I get set up? ##

1. Install PostgreSQL in your local environment -- OR you may use the current PostgreSQL instance on cloud (ElephantSQL)
1. Login to PostgreSQL and create new database named `cm-db`. Leave it empty for now.
1. Configuration 
   >* install and configure `JAVA_HOME` path in your local environment.
1. Dependencies 
   >* **Maven 3+**
   >* **JDK 8**
   >* **Anypoint Studio 7.2.1+ and Mulesoft 4.1.3+**
1. Import this repository to Anypoint Studio project
   >* Clone / Checkout this project to your local path
   >* Open Anypoint Studio, select `File -> Import...` then choose `Anypoint Studio project from File System` Next fill Project Root to your local path where you saved the Cloned repository
1. Execute DDL Scripts (Import database tables and initial Insert)
   >* You can do this simply just by running SQL files located in `src/main/resources/db` .
1. Database configuration
   >* please adjust the db connection to match your local DB configuration. You can do this by replacing the database configuration values in `src/main/mule/cm-api.xml` file:
      >* If you are using PostgreSQL in local environment, set the `db:generic-conection url="jdbc:postgresql://localhost:5432/cm_db?user=YourUser&password=YourPassword"`
	  >* If you want to use current PostgreSQL instance on ElephantSQL, set the `db:generic-conection url="jdbc:postgresql://salt.db.elephantsql.com:5432/nvwyeivz?user=nvwyeivz&amp;password=fbdkFl_XhFwZ3-YL2T6AhI_XG5kDyKxD"`
1. Deployment instructions

    + run as **Mule Application** in Anypoint Studio
        * select the project in Package Explorer, then right click, choose `Run As -> Mule Application`
    + deploy as Mule Deployable Archive **jar** file
        * select the project in Package Explorer, then right click, choose `Export -> Anypoint Studio Project to Mule Deployable Archive`, then deploy that jar to preconfigured Mule Server version 4.1.3 or newer. 
    + deploy to **CloudHub**
	    * make sure to use PostgreSQL instance on ElephantSQL with the correct `db:generic-conection` configuration
        * select the project in Package Explorer, then right click, choose `Anypoint Platform -> Deploy to CloudHub`, then deploy that jar to preconfigured Mule Server version 4.1.3 or newer. 


* * *

### Contribution guidelines ###

1. API Uses

    #### Postman ####
	
	Import the Postman Collection from `src/main/resources/api`

    > GET request : 

     `http://localhost:8081/cmAPI/getIdentity` OR `http://cm-api.us-e2.cloudhub.io/cmAPI/getIdentity`

    > GET by ID request : 

    `http://localhost:8081/cmAPI/getIdentity/f4fffe91-910a` OR `http://cm-api.us-e2.cloudhub.io/cmAPI/getIdentity/f4fffe91-910a`
	
	  * Example of Response:
	  
	  `{
  "Identification": {
    "ID": "f4fffe91-910a",
    "FirstName": "Bob",
    "LastName": "Frederick",
    "DOB": "06/21/1980",
    "Gender": "M",
    "Title": "Manager"
  },
  "Address": [
    {
      "type": "home",
      "number": 1234,
      "street": "Blah Blah St",
      "Unit": "1 a",
      "City": "Somewhere",
      "State": "WV",
      "zipcode": "12345"
    }
  ],
  "Communication": [
    {
      "type": "email",
      "value": "bfe@sample.com",
      "preferred": true
    },
    {
      "type": "cell",
      "value": "304-555-8282"
    }
  ]
}`

    > POST (Insert) new data :
	
	POST `http://localhost:8081/cmAPI/insertIdentity` OR `http://cm-api.us-e2.cloudhub.io/cmAPI/insertIdentity`
	
	> PUT (Update) data :
	
	UPDATE `http://localhost:8081/cmAPI/updateIdentity` OR `http://cm-api.us-e2.cloudhub.io/cmAPI/updateIdentity`
	
	> DELETE data :
	
	DELETE `http://localhost:8081/cmAPI/deleteIdentity` OR `http://cm-api.us-e2.cloudhub.io/cmAPI/deleteIdentity`
	

### Who do I talk to? ###

1. Repo owner : [ahmad@baihaki.net](mailto:ahmad@baihaki.net)
=======
